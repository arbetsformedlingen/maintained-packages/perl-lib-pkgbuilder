FROM docker.io/library/ubuntu:22.04


RUN apt-get -y update && apt-get -y install --no-install-recommends --no-install-suggests \
        dh-make-perl git devscripts \
        libmodule-build-perl libmoosex-classattribute-perl \
        libjson-any-perl libtest-exception-perl libjson-maybexs-perl \
        liburl-encode-perl liburi-template-perl libfile-slurper-perl \
        libtest-timer-perl libthrowable-perl libmodule-build-tiny-perl \
        libtest-pod-perl libppi-perl liblog-log4perl-perl \
        libfuture-perl libtest-identity-perl libtest-needs-perl \
        libmojolicious-perl libstring-crc32-perl \
        libdatetime-format-iso8601-perl libclass-unload-perl \
        libmodule-find-perl libfile-homedir-perl libtest-warnings-perl \
        build-essential libexpat1-dev libssl-dev libnet-ssleay-perl \
        libcrypt-ssleay-perl openssh-client libxml-parser-perl \
        libxml-sax-expat-perl libxml-simple-perl libfurl-perl \
        libio-socket-ssl-perl libmoose-perl libconfig-general-perl


# remove interactive questions and automatically install dependencies
RUN yes | perl -MCPAN -e 'my $c = "CPAN::HandleConfig"; $c->load(doit => 1, autoconfig => 1); $c->edit(prerequisites_policy => "follow"); $c->edit(build_requires_install_policy => "yes"); $c->commit'\
    && mkdir /out

WORKDIR /

COPY build.sh /build.sh

ENTRYPOINT [ "/build.sh", "--trg-dir", "/out" ]
