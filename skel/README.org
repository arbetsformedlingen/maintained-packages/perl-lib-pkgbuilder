* <library name>


** SYNOPSIS

: ./run.sh

No arguments are required. You will be prompted for a key fingerprint to sign the files with.


** DESCRIPTION

This is a Debian package builder for the CPAN library <CPAN module>

It is still far from perfect, but builds usable, signed packages with
correct copyright and license information.

When starting the program, you will be prompted for a gpg key
fingerprint to sign the files with. You can list your key fingerprints
with:
: gpg --list-secret-keys --with-subkey-fingerprints


** AUTHOR

Written by <your name>.


** REPORTING BUGS

help: L<your git repo>


** COPYRIGHT

<your name>
<your license>
