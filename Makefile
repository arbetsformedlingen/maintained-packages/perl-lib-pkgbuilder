# requires GNU Make - this Makefile won't work with BSD Make
.ONESHELL:
SHELL = /bin/bash


include pkgbuilder.settings


build-image:
	if ! $(CONTSYS) image inspect $(IMG) >/dev/null 2>/dev/null; then
		$(CONTSYS) build -t $(IMG) .
	fi


.PHONY:
clean:
	if $(CONTSYS) image inspect $(IMG) >/dev/null 2>/dev/null; then
		$(CONTSYS) image rm $(IMG)
	fi


.PHONY:
gendocs: build.sh
	pod2readme --no-backup build.sh
	mkdir -p docs
	pod2man build.sh > docs/build.sh.1
	pod2text build.sh > docs/build.sh.txt
	gzip -f docs/*.1


test:
	tmpd=$$(mktemp -d)
	cp skel/* $$tmpd
	cd $$tmpd || exit 1
	grep -q '<your name>' Makefile || exit 1
	sed -i 's|<your name>|Test Testsson|g' Makefile || exit 1
	sed -i 's|<your email>|email@email.se|' Makefile || exit 1
	sed -i 's|<CPAN module>|Net::Amazon::Signature::V4|' Makefile || exit 1
	sed -i 's|<CPAN module version>|0.21|' Makefile || exit 1
	sed -i 's|<set Perl library name here>|libnet-amazon-signature-v4-perl|g' Makefile || exit 1
	env SIGNKEY=EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE $(MAKE) initialise-debian-dir || exit 1
	ls -l $(PKGDIR)/*.deb || exit 1
	ls -l debian/copyright || exit 1
	echo TESTCOPYRIGHT >> debian/copyright
	rm -f $(PKGDIR)/*.deb
	env SIGNKEY=EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE $(MAKE) build-artefact
	dpkg-deb -x $(PKGDIR)/*.deb chk #|| exit 1
	grep TESTCOPYRIGHT chk/usr/share/doc/libnet-amazon-signature-v4-perl/copyright
	lintian $(PKGDIR)/*.deb #|| true



#### TOOL RULES TO CALL FROM OTHER MAKEFILES:

.PHONY:
chkkey:
	if [ "$$SIGNKEY" = "" ]; then
		echo "**** set SIGNKEY envvar before calling" >&2
		echo "**** (gpg --list-keys --with-subkey-fingerprints)" >&2
		exit 1
	fi


sign-pkg:
	cd ../$(PKGDIR)
	debsign -k"$$SIGNKEY" lib*.dsc lib*source.changes


.PHONY:
clean-pkg:
	if [ "$(CLEANDIR)" = "" ]; then echo "set CLEANDIR" >&2; exit 1; fi
	rm -rf $(CLEANDIR)/$(PKGDIR)/ $(CLEANDIR)/perl-lib-pkgbuilder
