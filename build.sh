#!/usr/bin/env bash
#
# File:           build.sh
#
# Author:         Per Weijnitz
# E-Mail:         per.weijnitz@arbetsformedlingen.se
# Org:            arbetsformedlingen.se
# License:        GPLv3
#

: <<=cut
=head1 build.sh

Run 'dh-make-perl' and make adjustments to the generated files.


=head1 SYNOPSIS

build.sh [ options ]

    --trg-dir            target directory for artefacts
    --version            version string
    --maintainer-email   maintainer email
    --maintainer-name    maintainer name
    --signkey            key to sign packages with
    --cpan-module        the CPAN module to build
    --cpan2deb-flags F   extra flags to cpan2deb (optional)
    --setupscript S      a script to be run to prepare the build environment (optional)
    --load-debian-dir D  copy the debian-directory from D (optional)

    # special options
    --save-debian-dir D  copy the debian-directory to D (optional)
                         - useful when setting up a new package
    --chown O            saved files will be chowned with O

=head1 DESCRIPTION

This is one way to construct a Debian package from CPAN module.


=head1 SETUP A NEW PERL PACKAGING PROJECT

 1. Copy the C<skel>-directory to form a new working directory.
 2. Populate Makefile and README.org with values where there are C<<placeholders>>.
 3. Run C<make initialise-debian-dir>
 4. Edit C<debian/copyright>


=head1 BUILD PACKAGES

 1. Run C<bash run.sh> to build package to C<pgks> directory.


=head1 AUTHOR

Written by Per Weijnitz.


=head1 REPORTING BUGS

build.sh help: L<https://gitlab.com/arbetsformedlingen/maintained-packages/perl-lib-pkgbuilder>


=head1 COPYRIGHT

Copyright 2021 Arbetsformedlingen.  License GPLv3+: GNU GPL version 3
or later <https://gnu.org/licenses/gpl.html>.  This is free software:
you are free to change and re distribute it. There is NO WARRANTY, to
the extent permitted by law.


=cut

set -eEu -o pipefail


TRGDIR=""
VER=""
MAINTEMAIL=""
MAINTNAME=""
SIGNKEY=""
CPANMODULE=""
CPAN2DEBFLAGS=""
SETUPSCRIPT=""
DEBIANDIR_TO=""
DEBIANDIR_FROM=""
CHOWN=""

set -x
while [ "$#" -gt 0 ]; do
    case "$1" in
        --trg-dir)          TRGDIR="$2"; shift;;
        --version)          VER="$2"; shift;;
        --maintainer-email) MAINTEMAIL="$2"; shift;;
        --maintainer-name)  MAINTNAME="$2"; shift;;
        --signkey)          SIGNKEY="$2"; shift;;
        --cpan-module)      CPANMODULE="$2"; shift;;
        --cpan2deb-flags)   CPAN2DEBFLAGS="$2"; shift;;
        --setupscript)      SETUPSCRIPT="$2"; shift;;
        --save-debian-dir)  DEBIANDIR_TO="$2"; shift;;
        --load-debian-dir)  DEBIANDIR_FROM="$2"; shift;;
        --chown)            CHOWN="$2"; shift;;
        --)                 break;;
        *)                  echo "**** unknown option $1" >&2; exit 1;;
    esac
    shift
done


function setup_from_cpan() {
    local cpan="$1"
    local email="$2"
    local name="$3"
    local key="$4"
    local cpanflags="$5"

    env DEBFULLNAME="$name" cpan2deb $cpanflags --email "$email" make "$cpan"
    cd lib*-perl || exit 1

    echo "GENERATED COPYRIGHT:"
    cat debian/copyright
    echo

    # remove autogen blurb
    sed -i '/This description was automagically extracted from the module by dh-make-perl/ d' debian/control
    sed -i '$d' debian/control

    dpkg-buildpackage  -j -rfakeroot -k"$key" || exit 1

    cd ..
    rm -rf lib*perl
}



function setup_from_files() {
    local ver="$1"
    local key="$2"
    local trgdir="$3"
    local debiandir_from="$4"
    local debiandir_to="$5"

    dpkg-source -x lib*.dsc || exit 1

    cd lib*-perl-"$ver" || exit 1

    if [ -d "$debiandir_from" ]; then
        rm -rf debian/*
        cp -r "$debiandir_from"/* debian/
    fi

    dpkg-buildpackage       -j -rfakeroot -k"$key" || exit 1
    dpkg-buildpackage -S -d -j -rfakeroot -k"$key" || exit 1

    if [ -d "$debiandir_to" ]; then
        cp -r debian/* "$debiandir_to"/
    fi

    cd ..
    rm -rf lib*-perl-"$ver"
}


#### MAIN

if [ -f "$SETUPSCRIPT" ]; then
    bash "$SETUPSCRIPT" || exit 1
fi

mkdir pkgbuilder_work
cd pkgbuilder_work

if [ $(ls -1 "$TRGDIR"/*.dsc | wc -l) = 0 ]; then
    echo "**** setting up from CPAN" >&2
    setup_from_cpan "$CPANMODULE" "$MAINTEMAIL" "$MAINTNAME" "$SIGNKEY" "$CPAN2DEBFLAGS"
    cp -f lib*perl* "$TRGDIR"/
fi

cp -f "$TRGDIR"/* .
setup_from_files "$VER" "$SIGNKEY" "$TRGDIR" "$DEBIANDIR_FROM" "$DEBIANDIR_TO"
cp -f lib*-perl* "$TRGDIR"/


if [ "$CHOWN" != "" ]; then
   if [ -d "$DEBIANDIR_TO" ]; then
      chown -R "$CHOWN" "$DEBIANDIR_TO"/
   fi
   chown -R "$CHOWN" "$TRGDIR"/
fi

cd ..
rm -rf pkgbuilder_work
